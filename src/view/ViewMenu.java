package view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileNotFoundException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import controller.Information;
import controller.Menu;


/**
 * Vue instanciant le menu permettant d'acc�der la vue changeant les param�tres, les informations, et lancer une partie
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class ViewMenu extends JFrame {


	private static final long serialVersionUID = 1L;
	private Menu m;
	
	/**
	 * Constructeur de ViewMenu
	 * @param _m Une instance menu permettant d'acc�der aux fonctions
	 */
	public ViewMenu(Menu _m) {
		m = _m;
		
		// Initialisation des caract�ristiques de la fen�tre
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setTitle("Jeu QUARTO!");
		setLocation(100,100);
		setPreferredSize(new Dimension(800,450));
		setResizable(false);
		addWindowListener(new QuitterProgFen(this));
				
					/** Cr�ation du MENU **/
		JMenuBar menuBar = new JMenuBar();
		
		/** Cr�ation du menu aide **/
		JMenu menuAide = new JMenu("?");
		JMenuItem fonctionnement = new JMenuItem("Guide d'utilisation",KeyEvent.VK_G);
		JMenuItem informationAppli = new JMenuItem("R�gles du jeu",KeyEvent.VK_R);
		
		// Ajout des listeners
		fonctionnement.addActionListener(new FonctionnementAction(this));
		informationAppli.addActionListener(new ReglesDuJeuAction(this));
		
		// Ajout au menu
		menuAide.add(fonctionnement);
		menuAide.add(informationAppli);
		
		/** Cr�ation du menu Jeu **/
		JMenu menuJeu = new JMenu("Jeu");
		JMenuItem nouvellePartie = new JMenuItem("Nouvelle partie",KeyEvent.VK_P);
		JMenuItem options = new JMenuItem("Param�tres",KeyEvent.VK_P);
		JMenuItem quitter = new JMenuItem("Quitter",KeyEvent.VK_Q);
		
		// Ajout des listeners
		nouvellePartie.addActionListener(new DemarrerPartieAction(this));
		options.addActionListener(new ModifierParametre(this));
		quitter.addActionListener(new QuitterProgAction(this));
		
		// Ajout au menu
		menuJeu.add(nouvellePartie);
		menuJeu.add(options);
		menuJeu.addSeparator();
		menuJeu.add(quitter);
		
		// Ajout � la barre principale
		menuBar.add(menuJeu);
		menuBar.add(menuAide);
		
					/** FIN CREATION MENU **/
		
		// Cr�ation du panel principal
		JPanel fond = new JPanel();
		fond.setLayout(null);
		
		// Cr�ation d'une image de fond
		JLabel im = new JLabel(new ImageIcon("images/fond.png"));
		im.setBounds(0, 0, 800, 400);
		
		// Cr�ation des boutons
		JButton newGame = new JButton("Nouvelle partie");
		JButton newParam = new JButton("Param�tres");
		newGame.setBounds(175, 275, 200, 40);
		newParam.setBounds(425, 275, 200, 40);
		
		// Ajout des listeners
		newGame.addActionListener(new DemarrerPartieAction(this));
		newParam.addActionListener(new ModifierParametre(this));
		
		// Ajout des composants au panel principal
		fond.add(newGame);
		fond.add(newParam);
		fond.add(im);
		
		// Ajout des composants principal � la fen�tre
		setJMenuBar(menuBar);
		setContentPane(fond); 
		
		pack(); 
		setVisible(true);
	}
	
	/**
	 * Getter menu
	 * @return
	 */
	public Menu getMenu() { return this.m; }
	
	/**
	 * Permet d'avoir une image en fond de composant
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class AfficheImage extends JPanel 
	{ 
		private static final long serialVersionUID = 1L;
		Image eau; 

		AfficheImage(String s) 
		{ 
			eau = getToolkit().getImage(s); 
		} 

		public void paintComponent(Graphics g) 
		{ 
			super.paintComponent(g); 
			g.drawImage(eau, 0, 0, getWidth(), getHeight(), this); 
		} 
	} 

	/**
	 * Listener sur le menu quitter pour quitter le programme
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class QuitterProgAction implements ActionListener {

		private ViewMenu view;
		
		public QuitterProgAction(ViewMenu _v) { this.view = _v; }
		@Override
		public void actionPerformed(ActionEvent e) {
			view.getMenu().quitterProg(view);
		}
		
	}

	/**
	 * Listener sur la fen�tre pour quitter le programme
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class QuitterProgFen implements WindowListener {
		private ViewMenu view;
		public QuitterProgFen(ViewMenu _v) { this.view = _v; }
		
		public void windowClosing(WindowEvent e) { view.getMenu().quitterProg(view); }
		
		public void windowOpened(WindowEvent e) {}
		public void windowClosed(WindowEvent e) {}
		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowActivated(WindowEvent e) {}
		public void windowDeactivated(WindowEvent e) {}
		
	}



	/**
	 * Listener sur le bouton modifier param�tres pour modifier ceux-ci
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class ModifierParametre implements ActionListener {
		private ViewMenu view;
		
		public ModifierParametre(ViewMenu _v) { this.view = _v; }

		public void actionPerformed(ActionEvent e) {
			view.getMenu().modifParam(view);
		}
	}
}



