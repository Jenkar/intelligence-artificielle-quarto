package view;

import javax.swing.*;

import controller.Joueur;
import controller.Partie;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import model.Pion;
import model.Plateau;

/**
 * Vue instanciant une partie avec le plateau, les pi�ces restantes et le coup courant
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class ViewGame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private JPanel boardPanel, pieceLeftPanel, informationPanel, globalPanel;
	private ArrayList<JLabel> board, pieceLeft;
	private Pion pieceToPlay;
	private int type;
	private JMenuItem annulerCoup;
	private Plateau actualBoard;
	private Partie partie;
	
	/**
	 * Constructeur de la vue du jeu
	 * @param _type Type du coup (ordinateur, s�lection, placement)
	 * @param _p Partie en cours
	 */
	public ViewGame (int _type, Partie _p) {
		
		// Initialisation des valeurs
		pieceToPlay = null;
		board = new ArrayList<JLabel>();
		pieceLeft = new ArrayList<JLabel>();
		partie = _p;
		
		// Initialisation des caract�ristiques de la fen�tre
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setTitle("Partie en cours");
		setLocation(20,20);
		setResizable(false);
		
		
					/*** Cr�ation du Menu ***/
		
		/** Menu aide **/
		// Cr�ation des items
		JMenu menuAide = new JMenu("?");
		JMenuItem fonctionnement = new JMenuItem("Guide d'utilisation",KeyEvent.VK_G);
		JMenuItem informationAppli = new JMenuItem("R�gles du jeu",KeyEvent.VK_R);
		
		// Ajout des listeners
		fonctionnement.addActionListener(new FonctionnementAction(this));
		informationAppli.addActionListener(new ReglesDuJeuAction(this));
		
		// Ajout des items au menu aide
		menuAide.add(fonctionnement);
		menuAide.add(informationAppli);
		
		/** Menu jeu **/
		// Cr�ation des items
		JMenu menuJeu = new JMenu("Jeu");
		JMenuItem nouvellePartie = new JMenuItem("Nouvelle partie",KeyEvent.VK_P);
		annulerCoup = new JMenuItem("Annuler coup",KeyEvent.VK_N);
		JMenuItem quitter = new JMenuItem("Retour au menu",KeyEvent.VK_Q);
		
		// Ajout des listeners
		nouvellePartie.addActionListener(new DemarrerPartieAction(this));
		annulerCoup.addActionListener(new AnnulerCoupAction(this));
		quitter.addActionListener(new QuitterJeuAction(this));
		addWindowListener(new QuitterJeuFen(this));
		
		// Ajout des items au menu jeu
		menuJeu.add(nouvellePartie);
		menuJeu.addSeparator();
		menuJeu.add(annulerCoup);
		menuJeu.addSeparator();
		menuJeu.add(quitter);
		
		/** Barre de menu **/
		JMenuBar menuBar = new JMenuBar();
		
		// Ajout des deux menus � la barre
		menuBar.add(menuJeu);
		menuBar.add(menuAide);
		
		// Ajout du menu � la fen�tre
		setJMenuBar(menuBar);
		
					/*** FIN CREATION MENU ***/
		
		
					/*** Cr�ation du contenu de la fen�tre ***/
		
		// Cr�ation du panel pour le plateau de jeu
		boardPanel = new JPanel(new GridLayout(4,4));
		
		GridBagConstraints a = new GridBagConstraints();
		a.gridy = 0;
		a.gridx = 0;
		a.insets = new Insets(10,10,10,10);
		a.fill = GridBagConstraints.HORIZONTAL;
	
		// Cr�ation du panel pour les pi�ces restantes
		pieceLeftPanel = new JPanel(new GridLayout(2,9));
		GridBagConstraints b = new GridBagConstraints();
		b.gridy = 1;
		b.gridx = 0;
		b.gridwidth = 2;
		b.insets = new Insets(0,10,10,10);
		b.fill = GridBagConstraints.HORIZONTAL;
		
		// Cr�ation du panel pour les informations
		informationPanel = new JPanel(new FlowLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		
		// Cr�ation du panel global qui contient les 3 autres panels
		globalPanel = new JPanel(new GridBagLayout());
		globalPanel.add(informationPanel,c);
		globalPanel.add(boardPanel,a);
		globalPanel.add(pieceLeftPanel,b);
		
		// On affecte le panel global � la fen�tre
		setContentPane(globalPanel);
		
					/*** FIN CREATION CONTENU JEU ***/
		
		// On met � jour la vue afin d'afficher le jeu original
		updateView(partie.getPlateau(),_type,null);
		
		pack(); 
		setVisible(true);
	}
	
	/**
	 * Met � jour la vue en fonction du plateau
	 * @param _board Plateau actuel
	 * @param _type Type du coup
	 * @param _pieceToPlay Pi�ce � jouer
	 */
	public void updateView (Plateau _board, int _type, Pion _pieceToPlay)  {
		/* On r�cup�re le type du coup :
		 * 0 : c'est au tour du joueur de choisir une pi�ce
		 * 1 : c'est au tour du joueur de placer une pi�ce
		 * 2 : c'est au tour de l'ordi de jouer
		 * 3 : partie termin�e
		 * 4 : partie gagn�e
		 */
		type = _type;
		
		// On r�cup�re le nouveau plateau
		actualBoard = _board;
		
		// On vide le contenu des panels
		informationPanel.removeAll();
		boardPanel.removeAll();
		pieceLeftPanel.removeAll();
			
		// On vide le contenu 
		pieceLeft.clear();
		board.clear();
			
		// On reconstitue le plateau de jeu
		for (int i=0;i<4;i++) {
			for (int j=0;j<4;j++) {
				if (_type == 4 && _board.getGagne().contains(_board.getBoard()[i][j]))
					board.add(new JLabel(new ImageIcon("images/"+_board.getBoard()[i][j].toString()+"win.png")));
				else
					board.add(new JLabel(new ImageIcon("images/"+_board.getBoard()[i][j].toString()+"board.png")));
				board.get(i*4+j).addMouseListener(new boardAction(this));
				boardPanel.add(board.get(i*4+j));	
			}
		}
		
		// On reconstitue les pi�ces qu'il reste � jouer
		pieceLeftPanel.add(new JLabel("<html><body><h3><font color='green'>Pions restants:</h3></body></html>"));
		
		int i=0, j=0;
		
		for(Pion res : _board.getPieceLeft())
		{
			// Permet de donner un affichage plus propre en affichant sur une ligne les pi�ces de la premi�re couleur restantes puis sur la ligne suivant les pi�ces de la seconde couleur
			if(res.getId()>8) {
				while(j<10) {
					pieceLeftPanel.add(new JLabel(""));
					j++;
				}
			}
			
			JLabel tmp = new JLabel(new ImageIcon("images/"+res.toString()+".png"));
			
			pieceLeft.add(tmp);
			pieceLeft.get(i).addMouseListener(new pieceLeftAction(this));
			pieceLeftPanel.add(pieceLeft.get(i));
			
			i++;
			j++;
		}
		
		while (j<18) { pieceLeftPanel.add(new JLabel("")); j++;}

		
		// On reconstitue l'information suivant l'action � effectuer
		switch(type)
		{
		case 0: // un joueur humain doit s�lectionner une pi�ce
			informationPanel.add(new JLabel("<html><body><h1><font color='red'>C'est � votre tour !<hr>S�lectionnez une pi�ce pour votre adversaire.</h1></body></html>"));
			break;
		case 1: // un joueur humain doit placer une pi�ce
			pieceToPlay = _pieceToPlay;
			informationPanel.add(new JLabel("<html><body><h1><font color='red'>C'est � votre tour !<hr>Vous jouez avec la pi�ce :</h1></body></html>\n"));
			informationPanel.add(new JLabel(new ImageIcon("images/"+pieceToPlay.toString()+"board.png")));
			break;
		case 2: // un ordinateur doit placer une pi�ce / en s�lectionner une
			informationPanel.add(new JLabel("<html><body><h1><font color='red'>C'est au tour de l'ordinateur.<hr>Veuillez patientier...</h1></body></html>"));
			break;
		case 3: // partie termin�e
			informationPanel.add(new JLabel("<html><body><h1><font color='green'>Partie termin�e !</h1></body></html>"));
			break;
		case 4:
			informationPanel.add(new JLabel("<html><body><h1><font color='green'>Partie termin�e !</h1></body></html>"));
			break;
		}
		
		// On permet d'annuler un coup � partir du moment o� deux coups ont �t� jou� au minimum, et que c'est au tour du joueur de placer une pi�ce
		if (partie.getPileCoup().isEmpty() || type==2 || type== 1)
			annulerCoup.setEnabled(false);
		else
			annulerCoup.setEnabled(true);
		
		// On redessine les �l�ments de la fen�tre
		informationPanel.revalidate();
		boardPanel.revalidate();
		pieceLeftPanel.revalidate();
		repaint();
	}
	
	/**
	 * Met � jour le plateau lors d'un placement de pi�ce lorsqu'une case est survol�e
	 * @param _indice Indice de la case � mettre � jour
	 * @param type 0 si on entre, 1 si on sort
	 */
	public void updateBoard(int _indice, int type) {
		
		// Permet de mettre � jour le plateau pour pr�visualiser o� la pi�ce sera jou�e
		if(type==0)
			board.get(_indice).setIcon(new ImageIcon("images/"+pieceToPlay.toString()+"board.png"));
		else if (getActualBoard().getBoard()[_indice/4][_indice%4].equals(new Pion(0)))
			board.get(_indice).setIcon(new ImageIcon("images/0board.png"));
		
		// On redessine le composant
		boardPanel.revalidate();
		repaint();
	}
	
	/**
	 * Getter pi�ce � jouer
	 * @return
	 */
	public Pion getPieceToPlay() { return this.pieceToPlay; }
	/**
	 * Getter liste des labels du plateau
	 * @return
	 */
	public ArrayList<JLabel> getBoard() { return this.board; }
	/**
	 * Getter liste des labels des pi�ces restantes
	 * @return
	 */
	public ArrayList<JLabel> getPieceLeft() { return this.pieceLeft; }
	/**
	 * Getter type de tour
	 * @return
	 */
	public int getTypeP() { return this.type; }
	/**
	 * Getter plateau de jeu
	 * @return
	 */
	public Plateau getActualBoard() { return this.actualBoard; }
	/**
	 * Getter partie en cours
	 * @return
	 */
	public Partie getPartie() { return this.partie; }
	
	
	/**
	 * Listener sur le panel du plateau pour appeler la fonction pla�ant un pion ou mettre � jour une case lorsqu'on la survole
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class boardAction implements MouseListener {
		private ViewGame view;
		
		public boardAction(ViewGame _v) { this.view = _v; }

		// Lorsque l'on clique sur une case, on va appeler une fonction permettant de placer le pion
		public void mouseClicked(MouseEvent e) {
			JLabel i = (JLabel) e.getSource();
			int ind = view.getBoard().indexOf(i);
			if (view.getTypeP()==1 && view.getActualBoard().getBoard()[ind/4][ind%4].equals(new Pion(0))) {
				try {
					new Joueur().placer(ind, view.getPartie());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		
		// Lorsque la souris entre dans une case, on met � jour le plateau en affichant l'image du pion � jouer
		public void mouseEntered(MouseEvent e) { 
			JLabel i = (JLabel) e.getSource();
			int ind = view.getBoard().indexOf(i);
			if (view.getTypeP()==1 && view.getActualBoard().getBoard()[ind/4][ind%4].equals(new Pion(0))) {
				view.updateBoard(ind, 0);
			}
		}

		// Lorsque la souris sors d'une une case, on remet le plateau dans son �tat initial
		public void mouseExited(MouseEvent e) {
			JLabel i = (JLabel) e.getSource();
			int ind = view.getBoard().indexOf(i);
			view.updateBoard(ind, 1);
		}
		
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
	}

	/**
	 * Listener sur le panel des pi�ces restantes pour appeler la s�lection d'une nouvelle pi�ce � jouer
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class pieceLeftAction implements MouseListener {

		private ViewGame view;
		
		public pieceLeftAction(ViewGame _v) { this.view = _v; }

		// Lorsque l'on clique sur une pi�ce, on va appeler une fonction permettant de s�lectionner cette pi�ce pour l'adversaire
		public void mouseClicked(MouseEvent e) {
			JLabel i = (JLabel) e.getSource();
			if (view.getTypeP()==0) {
				try {
					view.getPartie().pieceSelectionne(view.getPieceLeft().indexOf(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}

	/**
	 * Listener sur le menu pour annuler un coup
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class AnnulerCoupAction implements ActionListener {
		private ViewGame view;
		
		public AnnulerCoupAction(ViewGame _v) { this.view = _v; }
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				view.getPartie().annulerCoup();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Listener sur le menu quitter pour arr�ter le jeu
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class QuitterJeuAction implements ActionListener {
		private ViewGame view;
		
		public QuitterJeuAction(ViewGame _v) { this.view = _v; }

		public void actionPerformed(ActionEvent e) {
			view.getPartie().quitterJeu(view);
		}
	}

	/**
	 * Listener sur la fen�tre pour arr�ter le jeu
	 * @author Arnaud Quentin Yann Maxime
	 *
	 */
	class QuitterJeuFen implements WindowListener {
	private ViewGame view;
		
		public QuitterJeuFen(ViewGame _v) { this.view = _v; }

		public void windowClosing(WindowEvent e) {
			view.getPartie().quitterJeu(view);
		}

		public void windowClosed(WindowEvent e) {}
		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowActivated(WindowEvent e) {}
		public void windowDeactivated(WindowEvent e) {}
		public void windowOpened(WindowEvent e) {}
	}
}

