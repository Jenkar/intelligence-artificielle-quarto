package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.JFrame;

import controller.Information;

/**
 * Listener sur le menu r�gles de jeu pour afficher celles-ci
 * @author Arnaud Quentin Yann Maxime
 *
 */
class ReglesDuJeuAction implements ActionListener {
	private JFrame view;
	
	public ReglesDuJeuAction(JFrame _v) { this.view = _v; }
	
	public void actionPerformed(ActionEvent e) {
		try {
			Information.afficherRegles(view);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	
}

/**
 * Listener sur le menu fonctionnement pour d�crire le fonctionnement de l'application
 * @author Arnaud Quentin Yann Maxime
 *
 */
class FonctionnementAction implements ActionListener {
	private JFrame view;
	
	public FonctionnementAction(JFrame _v) { this.view = _v; }
	
	public void actionPerformed(ActionEvent e) {
		try {
			Information.afficherFonctionnement(view);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
}

/**
 * Listener sur le bouton d�marrer partie pour lancer une nouvelle partie
 * @author Arnaud Quentin Yann Maxime
 *
 */
class DemarrerPartieAction implements ActionListener {

	private JFrame view;
	
	public DemarrerPartieAction(JFrame _v) { this.view = _v;}
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			((ViewMenu) view).getMenu().lancerMenu(view);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}