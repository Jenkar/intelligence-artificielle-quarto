package view;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.Menu;
import model.Parametre;

/**
 * Vue permettant de modifier les param�tres d'une partie
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class ViewOption extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private JLabel description;
	private JComboBox<String> listeNiv;
	private Checkbox br1, br2, br3;
	private CheckboxGroup groupe;
	
	public ViewOption(JFrame _v, Parametre _p) {
		
		// Initialisation des caract�ristiques de la fen�tre
		super(JOptionPane.getFrameForComponent(_v));
		this.setModal(true);
		setLocationRelativeTo(_v);
		setPreferredSize(new Dimension(500,220));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setTitle("Modifier les param�tres d'une partie");
		setResizable(false);
		
		
		/** Panel s�lection de niveau de jeu **/
		// Initialisation des composants
		JPanel selectNiv = new JPanel(new GridLayout(3,1));
		JLabel infNiv = new JLabel("Niveau de jeu :");
		listeNiv = new JComboBox<String>(new String[]{"Niveau 1","Niveau 2","Niveau 3","Niveau 4"});
		description = new JLabel("");
		
		// Pr�-s�lection de l'item et affichage de l'information correspondante
		listeNiv.setSelectedIndex(_p.getNiv());
		updateInfo((String) listeNiv.getSelectedItem());
		
		// Ajout du listener
		listeNiv.addItemListener(new ChoixDifficulte(this));
		
		// Ajout des �l�ments au panel
		selectNiv.add(infNiv);
		selectNiv.add(listeNiv);
		selectNiv.add(description);
		
		/** Panel s�lection d'ordre de jeu **/
		JPanel selectOrdre = new JPanel(new GridLayout(2,3));
		JLabel jo = new JLabel("D�roulement d'une partie :");
		groupe=new CheckboxGroup();  
		br1 = new Checkbox("Je commence",groupe,_p.getOrdre()==0);
		br2 = new Checkbox("L'ordinateur commence",groupe,_p.getOrdre()==1);
		br3 = new Checkbox("Ordi vs Ordi",groupe,_p.getOrdre()==2);

		// Ajout des �l�ments au panel
		selectOrdre.add(jo);
		selectOrdre.add(new JLabel(""));
		selectOrdre.add(new JLabel(""));
		selectOrdre.add(br1);
		selectOrdre.add(br2);
		selectOrdre.add(br3);
		
		
		/** Cr�ation des bouttons **/
		JButton sauvegarder = new JButton("Sauvegarder");
		JButton annuler = new JButton("Annuler");
		
		// Ajout des listeners
		sauvegarder.addActionListener(new SauvegarderParam(this));
		annuler.addActionListener(new FermerBouton(this));
		
		
		/** Panel global de la fen�tre **/
		JPanel general = new JPanel(new FlowLayout());
		
		// Ajout des diff�rents �l�ments au panel g�n�ral 
		general.add(selectNiv);
		general.add(selectOrdre);
		general.add(sauvegarder);
		general.add(annuler);
		
		// Affectation du panel g�n�ral � la fen�tre et ajout du listener pour fermer la fen�tre
		setContentPane(general);
		addWindowListener(new FermerOption(this));
		
		
		pack(); setVisible(true);
	}
	
	/**
	 * Met � jour l'information suivant le niveau s�lectionn� dans la liste
	 * @param item Element s�lectionn� dans la liste d�roulante
	 */
	public void updateInfo(String item) {
		 switch(item) {
         case "Niveau 1":
       	  this.description.setText("Combinaisons : alignements");
       	  break;
         case "Niveau 2":
       	  this.description.setText("Combinaisons : alignements et petits carr�s");
       	  break;
         case "Niveau 3":
       	  this.description.setText("Combinaisons : alignements, petits et grands carr�s");
       	  break;
         case "Niveau 4":
       	  this.description.setText("Combinaisons : alignements, petits et grands carr�s, carr�s invers�s");
       	  break;
         }
	}
	
	/**
	 * Getter niveau
	 * @return
	 */
	public int getNiv() { 
		return this.listeNiv.getSelectedIndex();
	}
	
	/**
	 * Getter ordre
	 * @return
	 */
	public int getOrdre() {
		if (this.groupe.getSelectedCheckbox()==br1)
			return 0;
		if (this.groupe.getSelectedCheckbox()==br2)
			return 1;
		if (this.groupe.getSelectedCheckbox()==br3)
			return 2;
		
		return -1;
	}
	
	/**
	 * Listener sur la fen�tre pour fermer la popup
	 * @author Arnaud Quentin Maxime Yann
	 *
	 */
	class FermerOption implements WindowListener {
		private ViewOption view;
		
		public FermerOption(ViewOption _v) { this.view = _v; }

		public void windowClosing(WindowEvent e) {
			this.view.dispose();
		}

		public void windowOpened(WindowEvent e) {}
		public void windowClosed(WindowEvent e) {}
		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowActivated(WindowEvent e) {}
		public void windowDeactivated(WindowEvent e) {}
	}

	/**
	 * Listener sur la liste d�roulante pour mettre � jour l'information
	 * @author Arnaud Quentin Maxime Yann
	 *
	 */
	class ChoixDifficulte implements ItemListener {
		private ViewOption view;
		
		public ChoixDifficulte(ViewOption _v) { this.view = _v; }
		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
		         view.updateInfo((String) e.getItem());
			}
		}
	}

	/**
	 * Listener sur le bouton fermer pour fermer la popup
	 * @author Arnaud Quentin Maxime Yann
	 *
	 */
	class FermerBouton implements ActionListener {
	private ViewOption view;
		
		public FermerBouton(ViewOption _v) { this.view = _v; }

		@Override
		public void actionPerformed(ActionEvent e) {
			this.view.dispose();
		}
	}

	/**
	 * Listener sur le bouton sauvegarder pour sauvegarder les param�tres
	 * @author Arnaud Quentin Maxime Yann
	 *
	 */
	class SauvegarderParam implements ActionListener {
		private ViewOption view;
		
		public SauvegarderParam(ViewOption _v) {
			this.view = _v;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Menu.sauverParam(new Parametre(view.getNiv(),view.getOrdre()));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			this.view.dispose();
		}
	}
}

