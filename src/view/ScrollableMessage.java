package view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Vue permettant d'afficher le contenu d'un fichier texte en popup
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class ScrollableMessage
{
   
 public ScrollableMessage(String _m, JFrame _v, String _t, int _i) {

     JTextArea textArea = new JTextArea(20, 45);
     textArea.setText(_m);
     textArea.setEditable(false);
     textArea.setCaretPosition(0);
     
     JScrollPane scrollPane = new JScrollPane(textArea);

     JOptionPane.showMessageDialog(_v, scrollPane,_t,_i);
 }

}