package model;

/**
 * Instancie un pion avec ses caractéristiques
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class Pion {
	final int id; // Id du pion

	final boolean blanc; //le pion est il blanc?
	final boolean grand; //le pion est il grand?
	final boolean rond; //le pion est il rond?
	final boolean point; //le pion a t'il un point?
	
	//i => [1-16]
	 /**
	  * Constructeur de pion
	  * @param i Numéro du pion
	  */
	public Pion(int i){
		id = i;
		blanc = i<=8;
		grand = i%4==0 || i%4==3;
		rond = i%8>=1 && i%8<=4;
		point = i%2==0;
		
	}
	
	/**
	 * Getter de l'ID
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Test si deux pions sont égaux
	 */
	public boolean equals(Object p){
		return this.id == ((Pion)p).getId();
	}	

	/**
	 * Redéfinition de hashCode
	 */
	public int hashCode() { return this.id; }
	
	/**
	 * Test si 4 pions ont une caractéristique commune
	 * @param p0 Pion 0
	 * @param p1 Pion 1
	 * @param p2 Pion 2
	 * @param p3 Pion 3
	 * @return 0 si non, 1 si oui
	 */
	public static boolean commun(Pion p0, Pion p1, Pion p2, Pion p3){

		if(p0.id == 0 || p1.id == 0 || p2.id == 0 || p3.id == 0)
			return false;

		if(p0.blanc == p1.blanc && p0.blanc == p2.blanc && p0.blanc == p3.blanc
				|| p0.grand == p1.grand && p0.grand == p2.grand && p0.grand == p3.grand
				|| p0.rond == p1.rond && p0.rond == p2.rond && p0.rond == p3.rond
				|| p0.point == p1.point && p0.point == p2.point && p0.point == p3.point)
			return true;
		
		
		return false;
	}

	/**
	 * Retourne le nombre de caractéristiques communes entre 4 pions
	 * @param p0 Pion 0
	 * @param p1 Pion 1
	 * @param p2 Pion 2
	 * @param p3 Pion 3
	 * @return Le nombre de caractéristiques communes
	 */
	public static int nbConex(Pion p0, Pion p1, Pion p2, Pion p3){
		int res = 0;
		if ((p0 == null)){ // suppression des cases vides
			res = nbConex(p1,p2,p3);
		}else 
			if ((p1 == null)){ 
				res = nbConex(p0,p2,p3);
			}else 
				if ((p2 == null)){ 
					res = nbConex(p1,p0,p3);
				}else 
					if ((p3 == null)){ 
						res = nbConex(p1,p2,p0);
					}else { // cas : aucune case vide
						if(p0.blanc == p1.blanc && p0.blanc == p2.blanc && p0.blanc == p3.blanc)
							res++;
						if(p0.grand == p1.grand && p0.grand == p2.grand && p0.grand == p3.grand)
							res++;
						if(p0.rond == p1.rond && p0.rond == p2.rond && p0.rond == p3.rond)
							res++;
						if(p0.point == p1.point && p0.point == p2.point && p0.point == p3.point)
							res++;
					}
		return res;
	}

	/**
	 * Retourne le nombre de caractéristiques communes entre 3 pions
	 * @param p0 Pion 0
	 * @param p1 Pion 1
	 * @param p2 Pion 2
	 * @return Le nombre de caractéristiques communes
	 */
	public static int nbConex(Pion p0, Pion p1, Pion p2){
		int res = 0;
		if ((p0 == null)){ // suppression des cases vides
			res = nbConex(p1,p2);
		}else 
			if ((p1 == null)){ 
				res = nbConex(p0,p2);
			}else 
				if ((p2 == null)){ 
					res = nbConex(p1,p0);
				}else { // cas : aucune case vide
					if(p0.blanc == p1.blanc && p0.blanc == p2.blanc)
						res++;
					if(p0.grand == p1.grand && p0.grand == p2.grand)
						res++;
					if(p0.rond == p1.rond && p0.rond == p2.rond)
						res++;
					if(p0.point == p1.point && p0.point == p2.point)
						res++;
				}
		return res;
	}
	
	/**
	 * Retourne le nombre de caractéristiques communes entre 2 pions
	 * @param p0 Pion 0
	 * @param p1 Pion 1
	 * @return Le nombre de caractéristiques communes
	 */
	public static int nbConex(Pion p0, Pion p1){
		int res = 0;
		if ((p0 == null || p1 == null)){ // suppression des cases vides
			return 0;
		} else{ // cas : aucune case vide
			if(p0.blanc == p1.blanc)
				res++;
			if(p0.grand == p1.grand)
				res++;
			if(p0.rond == p1.rond)
				res++;
			if(p0.point == p1.point)
				res++;
		}
		return res;
	}
	
	/**
	 * Retourne l'id du pion en string
	 */
	public String toString() { return ""+this.id; }
}