package model;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Instancie un plateau de jeu avec le plateau et la réserve de pion
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class Plateau {
	
	private HashSet<Pion> reserve; // setGagne avec les pions non jouées
	private Pion[][] jeu; // tableau 4*4 représentant le plateau
	private Pion aJouer; // Pion sélectionné par le joueur précédent
	private ArrayList<Pion> gagne;
	private Pion dernierePieceJouee; //derniere piece jouée par le joueur
	private int dernierePieceJouee_x;
	private int dernierePieceJouee_y;

	/**
	 * setGagneter dernierePieceJouee
	 * @param piece
	 */
	public void setDernierePieceJouee(Pion piece){ this.dernierePieceJouee=piece;}
	/**
	 * setGagneter dernierePieceJoueex
	 * @param pos
	 */
	public void setDernierePieceJouee_x(int pos){ this.dernierePieceJouee_x=pos;}
	/**
	 * setGagneter dernierePieceJoueey
	 * @param pos
	 */
	public void setDernierePieceJouee_y(int pos){ this.dernierePieceJouee_y=pos;}
	/**
	 * Getter dernierePieceJouee
	 * @return
	 */
	public Pion getDernierePieceJouee(){return this.dernierePieceJouee;	}
	/**
	 * Getter dernierePieceJouee_x
	 * @return
	 */
	public int getDernierePieceJouee_x(){ return this.dernierePieceJouee_x;}
	/**
	 * Getter dernierePieceJouee_y
	 * @return
	 */
	public int getDernierePieceJouee_y(){ return this.dernierePieceJouee_y;	}
	/**
	 * Getter pièce à jouer
	 * @return
	 */
	public Pion getAJouer() { return this.aJouer; }
	/**
	 * Getter plateau de jeu
	 * @return
	 */
	public Pion[][] getBoard() { return this.jeu; }
	/**
	 * Getter pièces restantes
	 * @return
	 */
	public HashSet<Pion> getPieceLeft() { return this.reserve; }
	
	
	/**
	 */
	public Plateau(){
		reserve = new HashSet<Pion>();
		aJouer = new Pion(0);
		gagne = new ArrayList<Pion>();
		for(int i = 1; i<=16; i++){
			reserve.add(new Pion(i)); //Remplissage de la réserve
		}		
		jeu = new Pion[4][4]; // initialisation du plateau
		for(int i = 0; i<4; i++)
			for(int j = 0; j<4;j++)
				jeu[i][j] = new Pion(0);
		
		dernierePieceJouee_x=0;
		dernierePieceJouee_y=0;
		dernierePieceJouee=new Pion(0);
	}
	

	/**
	 * Retire un pion de la réserve
	 * @param p Pion à retirer
	 * @throws Exception
	 */
	public void oter(Pion p) throws Exception{
		if(reserve.contains(p))
			reserve.remove(p);
		else { throw new Exception("PionNotFound");}
	}

	/**
	 * Sélectionne un pion
	 * @param p Pion à sélectionner
	 * @throws Exception
	 */
	public void choisir(Pion p) throws Exception{
		oter(p);
		aJouer = p;
	}
	
	/**
	 * Place un pion sur le plateau
	 * @param x Coordonnée x
	 * @param y Coordonnée y
	 * @throws Exception
	 */
	public void placer(int x,int y) throws Exception{
		if (jeu[x][y].equals(new Pion(0))){ //si case libre
			jeu[x][y] = aJouer;
			this.dernierePieceJouee = aJouer;
			this.dernierePieceJouee_x = x;
			this.dernierePieceJouee_y = y;
			aJouer = null;
		}
		else
			throw new Exception("Case invalide x:"+x+" y:"+y+" pion:"+jeu[x][y]);
	}

	/**
	 * Test si le plateau est plein
	 * @return false si non, true si oui
	 */
	public boolean plein(){
		Pion p = new Pion(0);
		for(int i = 0; i < 4; i++)
			for(int j = 0; j < 4; j++)
				if(jeu[i][j].equals(p))
					return false;
		return true;
	}
	
	/**
	 * Test si la partie est finie
	 * @param _p Paramètres de la partie
	 * @return false si non, true si oui
	 */
	public boolean finie(Parametre _p) {
			return (alignement() && _p.getNiv()>=0) || 
					(petitCarre() && _p.getNiv()>=1) || 
					(grandCarre() && _p.getNiv()>=2) || 
					(losange() && _p.getNiv()==3);
	}
	
	/**
	 * Test si un alignement de 4 pions avec une caractéristique identique a été réalisé
	 * @return True si oui, false sinon
	 */
	public boolean alignement() {
		//récupere les x/y de la derniere piece jouée
		int x=getDernierePieceJouee_x();
		int y=getDernierePieceJouee_y();
			
		// test si un alignement horizontal ou vertical a été réalisé
		if(Pion.commun(jeu[0][y],jeu[1][y],jeu[2][y],jeu[3][y])) { 
			gagne = setGagne(jeu[0][y],jeu[1][y],jeu[2][y],jeu[3][y]);
			return true;
		}
		if(Pion.commun(jeu[x][0],jeu[x][1],jeu[x][2],jeu[x][3])) { 
			gagne = setGagne(jeu[x][0],jeu[x][1],jeu[x][2],jeu[x][3]);
			return true;
		}
		//test si un alignement diagonal a été réalisé
		if (x==y || (x+y)==3) {
			if(Pion.commun(jeu[0][0],jeu[1][1],jeu[2][2],jeu[3][3])) { 
				gagne = setGagne(jeu[0][0],jeu[1][1],jeu[2][2],jeu[3][3]);
				return true;
			}
			if(Pion.commun(jeu[3][0],jeu[2][1],jeu[1][2],jeu[0][3])) { 
				gagne = setGagne(jeu[3][0],jeu[2][1],jeu[1][2],jeu[0][3]);
				return true;
			}
		}

		return false;
	}
	
	/**
	 * Test si un grand carré de 4 pions avec une caractéristique identique a été réalisé
	 * @return True si oui, false sinon
	 */
	public boolean grandCarre() {
		if(Pion.commun(jeu[0][0],jeu[0][2],jeu[2][0],jeu[2][2])) { 
			gagne = setGagne(jeu[0][0],jeu[0][2],jeu[2][0],jeu[2][2]);
			return true;
		}
		if(Pion.commun(jeu[0][1],jeu[0][3],jeu[2][1],jeu[2][3])){ 
			gagne = setGagne(jeu[0][1],jeu[0][3],jeu[2][1],jeu[2][3]);
			return true;
		}
		if(Pion.commun(jeu[1][0],jeu[1][2],jeu[3][0],jeu[3][2])) { 
			gagne = setGagne(jeu[1][0],jeu[1][2],jeu[3][0],jeu[3][2]);
			return true;
		}
		if(Pion.commun(jeu[1][1],jeu[1][3],jeu[3][1],jeu[3][3])) { 
			gagne = setGagne(jeu[1][1],jeu[1][3],jeu[3][1],jeu[3][3]);
			return true;
		}
		return false;
	}
	
	/**
	 * Test si un petit carré de 4 pions avec une caractéristique identique a été réalisé
	 * @return True si oui, false sinon
	 */
	public boolean petitCarre() {
		if(Pion.commun(jeu[0][0],jeu[0][1],jeu[1][0],jeu[1][1])) { 
			gagne = setGagne(jeu[0][0],jeu[0][1],jeu[1][0],jeu[1][1]);
			return true;
		}
		if(Pion.commun(jeu[0][1],jeu[0][2],jeu[1][1],jeu[1][2])) { 
			gagne = setGagne(jeu[0][1],jeu[0][2],jeu[1][1],jeu[1][2]);
			return true;
		}
		if(Pion.commun(jeu[0][2],jeu[0][3],jeu[1][2],jeu[1][3])) { 
			gagne = setGagne(jeu[0][2],jeu[0][3],jeu[1][2],jeu[1][3]);
			return true;
		}
		if(Pion.commun(jeu[1][0],jeu[1][1],jeu[2][0],jeu[2][1])) { 
			gagne = setGagne(jeu[1][0],jeu[1][1],jeu[2][0],jeu[2][1]);
			return true;
		}
		if(Pion.commun(jeu[1][1],jeu[1][2],jeu[2][1],jeu[2][2])) { 
			gagne = setGagne(jeu[1][1],jeu[1][2],jeu[2][1],jeu[2][2]);
			return true;
		}
		if(Pion.commun(jeu[1][2],jeu[1][3],jeu[2][2],jeu[2][3])) { 
			gagne = setGagne(jeu[1][2],jeu[1][3],jeu[2][2],jeu[2][3]);
			return true;
		}
		if(Pion.commun(jeu[2][0],jeu[2][1],jeu[3][0],jeu[3][1])) { 
			gagne = setGagne(jeu[2][0],jeu[2][1],jeu[3][0],jeu[3][1]);
			return true;
		}
		if(Pion.commun(jeu[2][1],jeu[2][2],jeu[3][1],jeu[3][2])) { 
			gagne = setGagne(jeu[2][1],jeu[2][2],jeu[3][1],jeu[3][2]);
			return true;
		}
		if(Pion.commun(jeu[2][2],jeu[2][3],jeu[3][2],jeu[3][3])) { 
			gagne = setGagne(jeu[2][2],jeu[2][3],jeu[3][2],jeu[3][3]);
			return true;
		}
		return false;
	}
	
	/**
	 * Test si un losange de 4 pions avec une caractéristique identique a été réalisé
	 * @return True si oui, false sinon
	 */
	public boolean losange() {
		if(Pion.commun(jeu[0][1],jeu[2][1],jeu[1][0],jeu[1][2])) { 
			gagne = setGagne(jeu[0][1],jeu[2][1],jeu[1][0],jeu[1][2]);
			return true;
		}
		if(Pion.commun(jeu[0][2],jeu[2][2],jeu[1][1],jeu[1][3])) { 
			gagne = setGagne(jeu[0][2],jeu[2][2],jeu[1][1],jeu[1][3]);
			return true;
		}
		if(Pion.commun(jeu[1][1],jeu[3][1],jeu[2][0],jeu[2][2])) { 
			gagne = setGagne(jeu[1][1],jeu[3][1],jeu[2][0],jeu[2][2]);
			return true;
		}
		if(Pion.commun(jeu[1][2],jeu[3][2],jeu[2][1],jeu[2][3])) { 
			gagne = setGagne(jeu[1][2],jeu[3][2],jeu[2][1],jeu[2][3]);
			return true;
		}
		if(Pion.commun(jeu[0][1],jeu[1][3],jeu[2][0],jeu[3][2])) { 
			gagne = setGagne(jeu[0][1],jeu[1][3],jeu[2][0],jeu[3][2]);
			return true;
		}
		if(Pion.commun(jeu[0][2],jeu[1][0],jeu[2][3],jeu[3][1])) { 
			gagne = setGagne(jeu[0][2],jeu[1][0],jeu[2][3],jeu[3][1]);
			return true;
		}
		return false;
	}
	
	/**
	 * Récupère la liste des coups possibles
	 * @return Une liste contenant le numéro des cases où l'on peut jouer
	 */
	public ArrayList<Integer> getAllCoup() {
		ArrayList<Integer> tmp = new ArrayList<Integer>();
		
		// Chaque case vide du plateau (Pion 0) correspond à un coup jouable
		for(int x=0;x<4;x++) {
			for (int y=0;y<4;y++) {
				if (jeu[x][y].getId()==0) {
					tmp.add(x*4+y);
				}
			}
		}
		return tmp;
	}
	
	/**
	 * Annule la sélection d'un pion
	 * @param _p Pion à annuler
	 */
	public void annulerChoix(Pion _p) {
		reserve.add(_p);
	}
	
	/**
	 * Annule le placement d'un pion
	 * @param _x Coordonnée x 
	 * @param _y Coordonée y
	 */
	public void annulerPlace(int _x, int _y) {
		aJouer = jeu[_x][_y];
		jeu[_x][_y] = new Pion(0);
	}
	
	/**
	 * Créer la liste des pions réalisant la combinaison gagnante
	 * @param p0 Pion gagnant 0
	 * @param p1 Pion gagnant 1
	 * @param p2 Pion gagnant 2
	 * @param p3 Pion gagnant 3
	 * @return La liste des pions gagnants
	 */
	public ArrayList<Pion> setGagne (Pion p0, Pion p1, Pion p2, Pion p3) {
		ArrayList<Pion> ret = new ArrayList<Pion>();
		
		ret.add(p0);
		ret.add(p1);
		ret.add(p2);
		ret.add(p3);
		
		return ret;
	}
	
	/**
	 * Getter liste des pions gagbants
	 * @return
	 */
	public ArrayList<Pion> getGagne() { return this.gagne; }
}