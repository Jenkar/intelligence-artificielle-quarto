package model;

/**
 * Permet de g�rer les param�tres d'une partie
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class Parametre {

	private int niv, ordre;
	
	/**
	 * Constructeur de param�tres
	 * @param _n Niveau de jeu
	 * @param _o Ordre de jeu
	 */
	public Parametre (int _n, int _o) {
		this.niv = _n;
		this.ordre = _o;
	}

	/**
	 * Getter du niveau de jeu
	 * @return
	 */
	public int getNiv() { return this.niv; }
	
	/**
	 * Getter de l'ordre de jeu
	 * @return
	 */
	public int getOrdre() { return this.ordre;}
	
	/**
	 * Retourne une cha�ne de caract�re prenant le niveau de jeu et l'ordre de jeu
	 */
	public String toString() { return ""+this.getNiv()+this.getOrdre(); }
}
