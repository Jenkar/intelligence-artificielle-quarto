package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import model.Parametre;
import model.Pion;
import model.Plateau;

/**
 * Instancie l'algorithme minmax et l'�lagage alpha/b�ta pour le jeu QUARTO!
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class Minmax extends Thread{
	private Parametre pm;
	private Plateau pl;
	private int t;
	private Partie partie;
	private int prof;
	private Pion pion_courant;
	private int meilleur_choix;
	/**
	 * Constructeur de minmax
	 * @param _t Type du coup
	 * @param _p Partie en cours
	 * @throws Exception
	 */
	public Minmax(int _t, Partie _p) throws Exception {
		this.pm = _p.getParam();
		this.pl = _p.getPlateau();
		this.t = _t;
		this.partie = _p;
	}
	
	/**
	 * Permet d'ex�cuter l'algorithme minmax
	 */
	public void run() { try {
		// Suivant le nombre de coups restants, on modifie la profondeur de la recherche pour ne pas passer trop de temps
		prof=0;
		if(pl.getAllCoup().size()>11)
			prof=3;
		else if (pl.getAllCoup().size()>8)
			prof=4;
		else if (pl.getAllCoup().size()>6)
			prof=6;
		else
			prof=10;
		
		jouer(prof,t);
	} catch (Exception e) {
		e.printStackTrace();
	} }
	
	/**
	 * D�marre l'algorithme minmax suivant _t
	 * @param _pr Profondeur actuelle
	 * @param _t Type du coup (s�lection pi�ce, placement pi�ce)
	 * @throws Exception
	 */
	public void jouer (int _pr, int _t) throws Exception {
		
		int max_val = -999999, meilleur_coup=0;
		meilleur_choix=-999999;
		// Cas o� on doit placer un pion puis s�lectionner une pi�ce
		if(_t==0 || _t==2) {
			
			// On r�cup�re la liste de tous les coups disponibles pour 
			ArrayList<Integer> coups = pl.getAllCoup();
			
			// Dans le cas o� deux coups sont aussi int�ressant l'un que l'autre, nous permettra de choisir de fa�on random quel coup prendre
			int nbR = coups.size();
			
			// On va jouer tous les coups possibles
			for(int i=0;i<coups.size();i++) {
				
				pl.placer(coups.get(i)/4,coups.get(i)%4);
				
					
			    // On va commencer � boucler avec le plateau courant, apr�s avoir test� des coups
			    int val = min(_pr-1,max_val);
					
			 // On annule le dernier coup jou� pour revenir 
		    	pl.annulerPlace(coups.get(i)/4,coups.get(i)%4);
		    	
			    // Si le coup est plus int�ressant, on le r�cup�re et on met � jour alpha
			    if (val > max_val) {
			    	max_val = val;
			    	meilleur_coup = coups.get(i);
			    }
			    		// Si on obtient la m�me valeur, alors on choisira ce coup ou celui d'avant
			    		else if (val==max_val)
			    		{
			    			Random r = new Random();
			    			if(r.nextInt(nbR)==1)
			    			{
			    				meilleur_coup = coups.get(i);
			    			}
			    		}
			}
			// On place le pion que l'on doit jouer � l'endroit le plus propice
			pl.placer(meilleur_coup/4, meilleur_coup%4);
			
			// Si la partie n'est pas finie, on s�lectionne aussi le meilleur pion d�cid�
			if (!(partie.getPlateau().finie(pm) || partie.getPlateau().plein())) {
				pl.choisir(pion_courant);
				partie.getPileCoup().push(pl.getAJouer());
			}
		}

		// Cas o� on commence par s�lectionner une pi�ce... on la prends random
		if (_t==1 || _t==3) {
			Random r = new Random();
			int pick = r.nextInt(16);
			int i =1;
			for(Pion p : pl.getPieceLeft()) {
				if (i==pick) {
					pl.choisir(p);
					break;
				}
				
				i++;
			}
			partie.getPileCoup().push(pl.getAJouer());
		}
		
		
		// On v�rifie que la partie n'est pas termin�e
		if (!partie.partieTerminee(new IA(), partie.getVue())) {
			// Si jamais elle n'est pas termin�e, et que c'est une partie Joueur vs Ordi, on donne la main au joueur
			if(_t<2)
				partie.getVue().updateView(pl, 1, pl.getAJouer()); 
			// Sinon on est dans un cas ordi - ordi, on va relancer un minmax apr�s avoir mis � jour la vue
			else {
				partie.deuxOrdis();
			}
		}
		System.out.println("/*****/");
	}

	/**
	 * Fonction min
	 * @param _pr Profondeur actuelle
	 * @return La valeur la plus petite
	 * @throws Exception
	 */
	public int min (int _pr, int coupure) throws Exception {
		// Si on arrive sur une partie termin�e ou � la fin de la recherche, on �value la position actuelle
		if (_pr==0 || pl.finie(this.pm) || pl.plein()) {
			int eval = partie.getOrdi().eval(0,partie);
			
			return eval;
		}

		int plus_mauvais=-99999,inter=99999;
		
		// Ces valeurs nous permettent de r�cup�rer la derni�re pi�ce jou�e pour remettre dans l'�tat initial le plateau lorsque l'on a test� tous les coups
		int ox=pl.getDernierePieceJouee_x(),oy=pl.getDernierePieceJouee_y();
		Pion opd=pl.getDernierePieceJouee();
		
		// Nous permet de garder en m�moire le pion s�lectionn�, pour par la suite annuler et remettre celui-ci dans les pions restants
		Pion op=null;
		
		
		Set<Pion> malistCopie = new HashSet<Pion>();
		malistCopie.addAll(Collections.synchronizedSet(pl.getPieceLeft()));
		
		ArrayList<Integer> coups = pl.getAllCoup();
		
		synchronized(malistCopie){ 
			Iterator<Pion> iterator = malistCopie.iterator(); 
       
			while (iterator.hasNext()) { 
    	   
				// On s�lectionne le pion � jouer courant
				op = iterator.next();
				pl.choisir(op);
				
				// Nous permet de quitter la boucle si on n'est plus int�ress� par les valeurs
				boolean rester=true;
				
				// A nouveau, on essaye tous les coups
				for (int i=0;rester && i<coups.size();i++) {
			
					// On place le coup courant
					pl.placer(coups.get(i)/4,coups.get(i)%4);
				
					// On continue � descendre dans l'arbre..
					int val = max(_pr-1,plus_mauvais*(-1));
					
					// On annule la s�lection de la pi�ce
					pl.annulerPlace(coups.get(i)/4,coups.get(i)%4);
					
					// C'est la simulation des coups du joueur.
					// Plus la valeur est petite, plus c'est int�ressant pour lui; il va donc �videmment la r�cup�rer (c'est un dieu, apr�s tout)
					if (val < inter)
					{
						inter = val;
						
						// Si le coup qu'il vient de jouer est meilleur que le plus mauvais des meilleurs coups jou� jusqu'� maintenant
	    				// alors la suite des coups n'int�resse pas
						if (inter <= coupure) {
							// On revient dans la position initiale
							rester = false;
						}
					}
					
					// On revient dans la position initiale
					pl.setDernierePieceJouee(opd);
					pl.setDernierePieceJouee_x(ox);
					pl.setDernierePieceJouee_y(oy);
				}
				
				// Ici, c'est nous (ordinateur) qui regardons :
				// Si je s�lectionne ce pion, qu'est ce que le joueur va jouer.
				// On arrive ici avec le meilleur coup pour cette s�lection de pion
				// Je regarde maintenant si, en s�lectionnant un autre pion, l'ordinateur a jou� au mieux un coup moins int�ressant.
				// Si le coup qu'il jouerai avec la s�lection courante est plus mauvais, alors on r�cup�re celui-ci.
				if (inter > plus_mauvais) {
					plus_mauvais = inter;
					
					// Si on est au second niveau, on regarde cette valeur avec la valeur la plus mauvaise que l'on avait dans les branches pr�c�dentes
					// Si elle est sup�rieure, alors ce pion est moins int�ressant pour le joueur, donc on le garde en m�moire
					if(_pr==1 && meilleur_choix<plus_mauvais) {
						
						meilleur_choix = plus_mauvais;
						
						pion_courant = new Pion(op.getId());
					}
				}
				// On annule le coup pour revenir sur le plateau initial
				pl.annulerChoix(op);
			}
	    }
		
		return plus_mauvais;
	}

	/**
	 * Fonction max
	 * @param _pr La profondeur actuelle
	 * @return La valeur maximale
	 * @throws Exception
	 */
	public int max (int _pr, int coupure) throws Exception {
		// Si on arrive sur une partie termin�e ou � la fin de la recherche, on �value la position actuelle
		if (_pr==0 || pl.finie(this.pm) || pl.plein()) {
			int eval = partie.getOrdi().eval(1,partie);
			
			return eval;
		}
		
		// inter : valeur interm�diaire
		// plus_mauvais : 'pire des meilleurs' coup que l'on a r�alis� jusque l�
		int plus_mauvais = 999999, inter=-99999;
		
		// Ces valeurs nous permettent de r�cup�rer la derni�re pi�ce jou�e pour remettre dans l'�tat initial le plateau lorsque l'on a test� tous les coups
		int ox=pl.getDernierePieceJouee_x(),oy=pl.getDernierePieceJouee_y();
		Pion opd=pl.getDernierePieceJouee();
				
		// Nous permet de garder en m�moire le pion s�lectionn�, pour par la suite annuler et remettre celui-ci dans les pions restants
		Pion op=null;
		
		
		ArrayList<Integer> coups = pl.getAllCoup();
		Set<Pion> malistCopie = new HashSet<Pion>();
		malistCopie.addAll(Collections.synchronizedSet(pl.getPieceLeft()));
		
		// On va tenter la s�lection de tous les pions possible
		synchronized(malistCopie){ 
			Iterator<Pion> iterator = malistCopie.iterator(); 
       
			while (iterator.hasNext()) { 
				
				// On s�lectionne le pion suivant
				op = iterator.next();
				pl.choisir(op);
			
				// Nous permettra de sortir de la boucle si jamais les valeurs ne nous int�ressent plus
				boolean rester = true;
				
				// A nouveau, on essaye tous les coups possibles
				for (int i=0;rester && i<coups.size();i++) {
			
					// On simule le coup
					pl.placer(coups.get(i)/4,coups.get(i)%4);

					// On continue � descendre dans l'arbre..
	    			int val = min(_pr-1,plus_mauvais*(-1));
	    			
	    			// On annule le coup pour revenir sur le plateau initial
	    			pl.annulerPlace(coups.get(i)/4,coups.get(i)%4);
	    			
	    			// C'est la simulation de NOS coups. Plus la valeur est grande, mieux c'est, car on cherche le meilleur coup pour nous
	    			if (val > inter) {
	    				inter = val;
	    				// Si le coup que l'on vient de jouer est meilleur que le plus mauvais des meilleurs coups jou� jusqu'� maintenant
	    				// alors la suite des coups n'int�resse pas
	    				if (inter >= plus_mauvais) {
							rester = false;
						}	
	    			}
	    			
	    			// On revient sur la partie initiale avec aucun changement
	    			pl.setDernierePieceJouee(opd);
	    			pl.setDernierePieceJouee_x(ox);
	    			pl.setDernierePieceJouee_y(oy);
				}
		
				// Ici, c'est le joueur qui regarde :
				// Si il s�lectionne ce pion, qu'est ce que l'ordinateur va jouer.
				// On arrive ici avec le meilleur coup pour cette s�lection de pion
				// Le joueur regarde maintenant si, en s�lectionnant un autre pion, on aurait jouer au mieux un coup moins int�ressant.
				// Si le coup qu'on jouerai avec la s�lection courante est plus mauvais, alors l'ordi r�cup�re celui-ci.
				if (inter < plus_mauvais) {
					plus_mauvais = inter;
					
					// Si on est au second niveau, on regarde cette valeur avec la valeur la plus mauvaise que l'on avait dans les branches pr�c�dentes
					// Si elle est sup�rieure, alors ce pion est moins int�ressant pour le joueur, donc on le garde en m�moire
					if(_pr==1 && meilleur_choix<plus_mauvais) {
						meilleur_choix = plus_mauvais;
						
						pion_courant = new Pion(op.getId());
					}
				}
				
				// On annule la s�lection de la pi�ce
				pl.annulerChoix(op);
	    	}
	    }
		
		return plus_mauvais;
	}
}
