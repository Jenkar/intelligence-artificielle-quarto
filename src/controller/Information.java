package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JFrame;


import view.ScrollableMessage;

/**
 * Instancie une classe cr�ant des vues en r�cup�rant du texte d'un fichier texte
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class Information {
	/**
	 * Affiche les r�gles du jeu
	 * @param _v La fen�tre o� doit �tre centr� l'information
	 * @throws FileNotFoundException
	 */
	public static void afficherRegles(JFrame _v) throws FileNotFoundException {
		String regle ="";
		Scanner sc = new Scanner(new File("documentation/regles.txt"));
		while (sc.hasNextLine())
		{
		    regle+= sc.nextLine();
		    regle+="\n";
		}
		
		new ScrollableMessage(regle,_v,"R�gles du jeu",3);
		sc.close();
	}
	/**
	 * Affiche le fonctionnement de l'application
	 * @param _v La fen�tre o� doit �tre centr� l'information
	 * @throws FileNotFoundException
	 */
	public static void afficherFonctionnement(JFrame _v) throws FileNotFoundException {
		String fonction ="";
		Scanner sc = new Scanner(new File("documentation/fonctionnement.txt"));
		while (sc.hasNextLine())
		{
			fonction+= sc.nextLine();
		    fonction+="\n";
		}
		
		new ScrollableMessage(fonction,_v,"Fonctionnement de l'application",3);
		sc.close();
	}
}
