package controller;

import model.*;

/**
 * Instancie une IA jouant au jeu et se servant de minmax
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class IA extends Thread implements InstanceJouable {

	private int win = 200; 
	private int drawn = 199;
	
	/**
	 * Lance l'exécution de minmax pour déterminer les coups à jouer pour l'ordinateur
	 * @param _p Partie en cours
	 * @param _t Type du coup
	 */
	public void jouer(Partie _p, int _t) {
		Minmax t;
		try {
			t = new Minmax(_t,_p);
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Permet d'évaluer une position actuelle
	 * @param tour Tour du joueur = 1
	 * @param _p Partie en cours
	 * @return Le résultat de l'évaluation
	 */
	public int eval(int tour, Partie _p){
		Plateau p = _p.getPlateau();
		int res;
		int evalG=100;
		int conn = connexion(p,_p.getParam().getNiv()); 
		int a = 10;
		int b = -10;
		if (p.finie(_p.getParam())){
			evalG = win;
		}else if (p.plein()){
			evalG = drawn;
		}
		if (tour == 1){
			evalG = (evalG)*(-1); //complémentarisation si c'est le tour du joueur
			b=10;
		}
		res = evalG*a + b*conn; //evaluation
		
		return res;
	}
	
	/**
	 * Retourne le nombre de connexions de pièces similaires suivant le mode de getBoard()
	 * @param p1 Le plateau de getBoard()
	 * @return Le nombre de connexions
	 */
	public static int connexion(Plateau p1, int lvl){
		int max = 0;

		max = max+ Pion.nbConex(p1.getBoard()[0][0],p1.getBoard()[1][1],p1.getBoard()[2][2],p1.getBoard()[3][3]);
		max = max+Pion.nbConex(p1.getBoard()[0][3],p1.getBoard()[1][2],p1.getBoard()[2][1],p1.getBoard()[3][0]);
		
		for (int i =0; i<=3;i++){
			max = max+ Pion.nbConex(p1.getBoard()[i][0],p1.getBoard()[i][1],p1.getBoard()[i][2],p1.getBoard()[i][3]);
			max = max+ Pion.nbConex(p1.getBoard()[0][i],p1.getBoard()[1][i],p1.getBoard()[2][i],p1.getBoard()[3][i]);
		}
		if (lvl > 1){
			max = max+ Pion.nbConex(p1.getBoard()[0][0],p1.getBoard()[0][1],p1.getBoard()[1][0],p1.getBoard()[1][1]);
			max = max+ Pion.nbConex(p1.getBoard()[0][1],p1.getBoard()[0][2],p1.getBoard()[1][1],p1.getBoard()[1][2]);
			max = max+ Pion.nbConex(p1.getBoard()[0][2],p1.getBoard()[0][3],p1.getBoard()[1][2],p1.getBoard()[1][3]);
			max = max+ Pion.nbConex(p1.getBoard()[1][0],p1.getBoard()[1][1],p1.getBoard()[2][0],p1.getBoard()[2][1]);
			max = max+ Pion.nbConex(p1.getBoard()[1][1],p1.getBoard()[1][2],p1.getBoard()[2][1],p1.getBoard()[2][2]);
			max = max+ Pion.nbConex(p1.getBoard()[1][2],p1.getBoard()[1][3],p1.getBoard()[2][2],p1.getBoard()[2][3]);
			max = max+ Pion.nbConex(p1.getBoard()[2][0],p1.getBoard()[2][1],p1.getBoard()[3][0],p1.getBoard()[3][1]);
			max = max+ Pion.nbConex(p1.getBoard()[2][1],p1.getBoard()[2][2],p1.getBoard()[3][1],p1.getBoard()[3][2]);
			max = max+ Pion.nbConex(p1.getBoard()[2][2],p1.getBoard()[2][3],p1.getBoard()[3][2],p1.getBoard()[3][3]);
			
			if(lvl > 2){
				max = max+ Pion.nbConex(p1.getBoard()[0][0],p1.getBoard()[0][2],p1.getBoard()[2][0],p1.getBoard()[2][2]);
				max = max+ Pion.nbConex(p1.getBoard()[0][1],p1.getBoard()[0][3],p1.getBoard()[2][1],p1.getBoard()[2][3]);
				max = max+ Pion.nbConex(p1.getBoard()[1][0],p1.getBoard()[1][2],p1.getBoard()[3][0],p1.getBoard()[3][2]);
				max = max+ Pion.nbConex(p1.getBoard()[1][1],p1.getBoard()[1][3],p1.getBoard()[3][1],p1.getBoard()[3][3]);
				
				if (lvl > 3){
					max = max+ Pion.nbConex(p1.getBoard()[0][1],p1.getBoard()[2][1],p1.getBoard()[1][0],p1.getBoard()[1][2]);
					max = max+ Pion.nbConex(p1.getBoard()[0][2],p1.getBoard()[2][2],p1.getBoard()[1][1],p1.getBoard()[1][3]);
					max = max+ Pion.nbConex(p1.getBoard()[1][1],p1.getBoard()[3][1],p1.getBoard()[2][0],p1.getBoard()[2][2]);
					max = max+ Pion.nbConex(p1.getBoard()[1][2],p1.getBoard()[3][2],p1.getBoard()[2][1],p1.getBoard()[2][3]);
					max = max+ Pion.nbConex(p1.getBoard()[0][1],p1.getBoard()[1][3],p1.getBoard()[2][0],p1.getBoard()[3][2]);
					max = max+ Pion.nbConex(p1.getBoard()[0][2],p1.getBoard()[1][0],p1.getBoard()[2][3],p1.getBoard()[3][1]);
				}
			}
		}
		return max;
	}

	/**
	 * Retourne l'identité de l'instance jouable
	 */
	public String quiSuisJe() {
		return "Dommage, l'ordinateur a gagné !";
	}

}