package controller;

/**
 * Permet d'instancier un joueur pour jouer des coups sur un plateau de jeu
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class Joueur implements InstanceJouable {
	
	/**
	*	Permet de mettre � jour la vue pour placer un pion
	*/
	public void choisir(Partie _p) {
		// On met � jour la vue pour autoriser un joueur � placer un pion
		_p.getVue().updateView(_p.getPlateau(), 1, null);
	}
	
	/**
	*	Place la pi�ce � jouer sur le plateau
	*	@param indice Case s�lectionn�e
	*/
	public void placer(int indice, Partie _p) throws Exception {
		// On place le pion � la n�me case du plateau
		_p.getPlateau().placer(indice/4, indice % 4);
		// On met � jour la vue pour autoriser un joueur � s�lectionner un pion
		_p.getVue().updateView(_p.getPlateau(), 0, null);
		// On test si la partie est termin�e
		_p.partieTerminee(this,_p.getVue());
	}

	/**
	*	Retourne un string d�terminant l'instance
	*	@return String signifiant que c'est un joueur
	*/
	public String quiSuisJe() {
		return "Bravo, vous avez gagn� !";
	}
	
	/**
	*	Retourne le type de l'instance jouable
	*	@return Un entier identifiant le type
	*/
	public int getType() { return -1; }
}
