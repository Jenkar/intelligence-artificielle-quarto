package controller;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import view.ViewGame;
import view.ViewMenu;
import model.*;

/**
 * Classe principale du contr�leur permettant d'instancier une partie de jeu et de communiquer avec la vue
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class Partie {

	private Plateau p;
	private IA j1;
	private ViewGame vue;
	private Stack<Pion> pileCoup;
	private Parametre param;
	
	/**
	 * Constructeur de Partie
	 * @throws Exception
	 */
	public Partie() throws Exception {
		j1 = new IA();
		p = new Plateau();
		pileCoup = new Stack<Pion>();
		setParam(Menu.chargerParam());
		
		
		if (getParam().getOrdre()==0) {
			// Si lejoueur commence, on lui donne la main pour s�lectionner une pi�ce
			vue = new ViewGame(0,this);
		}
		else {
			// Si l'ordinateur commence, on lance un minmax pour s�lectionner une pi�ce (sera al�atoire car aucune incidence..)s
			vue = new ViewGame(0,this);
			vue.updateView(p, 2, null);
			if (getParam().getOrdre()==1)
				j1.jouer(this, 1);
			else
				j1.jouer(this, 3);
		}
	}
	
	/**
	 * Test si une partie est termin�e
	 * @param j Derni�re instance � avoir jou�e
	 * @param _v Fen�tre sur laquelle centrer la pop-up
	 * @return True si finie, false sinon
	 * @throws Exception
	 */
	public boolean partieTerminee(InstanceJouable j, JFrame _v) throws Exception {
		boolean finie = p.finie(getParam());
		boolean plein = p.plein();
		// On regarde si la partie est termin�e (plus de coup possible ou combinaison gagnante)
		if (finie || plein) {
			int option;
			if(finie)
			{
				vue.updateView(p,4,null);
				option = JOptionPane.showConfirmDialog(_v, j.quiSuisJe()+"\nVoulez-vous rejouer une partie ?", "Partie termin�e", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			}		
			else {
				vue.updateView(p,3,null);
				option = JOptionPane.showConfirmDialog(_v, "Egalit� !\nVoulez-vous rejouer une partie ?", "Partie termin�e", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			}
				
			// On relance une partie ou on retourne au menu
			if(option == JOptionPane.OK_OPTION){
				vue.dispose();
				new Partie();	
			}
			else
			{
				vue.dispose();
				new ViewMenu(new Menu());
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Permet de s�lectionner une pi�ce, puis donne la main � l'ordinateur
	 * @param indice Indice de la pi�ce s�lectionn�e
	 * @throws Exception
	 */
	public void pieceSelectionne(int indice) throws Exception {
		
		int i=0;
		for (Pion res : p.getPieceLeft())
		{
			if (i==indice)	{
				// On met � jour la pi�ce � jouer
				p.choisir(res);
				pileCoup.push(p.getAJouer());
				
				// On met � jour la vue et on lance minmax pour d�terminer les coups de l'ordinateur
				vue.updateView(p, 2, null);
				j1.jouer(this, 0);
				break;
			}
			i++;
		}
	}
	
	/**
	 * Annule les deux derniers coups jo�us
	 * @throws Exception
	 */
	public void annulerCoup() {
		// On r�cup�re les deux derniers coups jou�s
		Pion coup1 = pileCoup.pop();
		Pion coup2 = pileCoup.pop();
		
		// On vide les cases o� avait �t� jou�s les pions
		for (int i=0;i<16;i++) {
			if(p.getBoard()[i/4][i%4].equals(coup1) || p.getBoard()[i/4][i%4].equals(coup2))
				p.getBoard()[i/4][i%4] = new Pion(0);
		}
		
		// On remet les deux coups jou�s dans la r�serve
		p.getPieceLeft().add(coup1);
		p.getPieceLeft().add(coup2);
		
		// On met � jour la vue
		vue.updateView(p, 0, p.getAJouer());
	}
	
	/**
	 * Retourne la pile de coups
	 * @return
	 */
	public Stack<Pion> getPileCoup() { return pileCoup; }
	
	/**
	 * Quitte une partie en cours et retourne au menu
	 * @param _v Fen�tre sur laquelle centrer la pop-up
	 */
	public void quitterJeu(ViewGame _v) {
		int rep=JOptionPane.showConfirmDialog(_v,"Veux-tu vraiment arr�ter la partie et retourner au menu principal ?","Quitter la partie",JOptionPane.YES_NO_OPTION);	
		if(rep == 0) {
			_v.dispose();
			new ViewMenu(new Menu());
		}
	}
	
	/**
	 * Joue une partie avec deux ordinateurs
	 * @throws Exception
	 */
	public void deuxOrdis() throws Exception {
		// Met � jour la vue
		vue.updateView(p, 2, null);
		
		// Ex�cute un choix de pi�ces pour l'ordinateur
		j1.jouer(this, 2);
	}

	/**
	 * Getter des param�tres de la partie
	 * @return
	 */
	public Parametre getParam() {
		return param;
	}

	/**
	 * Setter des param�tres de la partie
	 * @param param
	 */
	public void setParam(Parametre param) {
		this.param = param;
	}
	
	/**
	 * Getter de la vue de la partie
	 * @return
	 */
	public ViewGame getVue() { return this.vue; }
	
	/**
	 * Getter du plateau de la partie
	 * @return
	 */
	public Plateau getPlateau() { return this.p; }
	
	/**
	 * Getter de l'IA de la partie
	 * @return
	 */
	public IA getOrdi() { return this.j1; }
}

