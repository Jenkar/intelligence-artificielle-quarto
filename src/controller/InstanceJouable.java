package controller;

/**
 * Instancie une classe que IA et Joueur h�rite afin de faciliter l'impl�mentation
 * @author Arnaud Quentin Yann Maxime
 *
 */

public interface InstanceJouable {
	/**
	 * Retourne un string suivant l'identit� de l'instance (joueur/ordi)
	 * @return
	 */
	public String quiSuisJe();
}
