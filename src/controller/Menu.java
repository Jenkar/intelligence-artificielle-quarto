package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.Parametre;
import view.ViewMenu;
import view.ViewOption;

/**
 * Classe principal du contr�leur contenant le main pour communiquer avec la vue mais aussi de g�rer les param�tres
 * @author Arnaud Quentin Yann Maxime
 *
 */
public class Menu {
	/**
	 * Main g�n�ral
	 * @param args
	 */
	public static void main(String[] args) {
		new ViewMenu(new Menu());
	}
	
	/**
	 * Lance une nouvelle partie
	 * @param _v Menu � fermer
	 * @throws Exception
	 */
	public void lancerMenu(JFrame _v) throws Exception {
		_v.dispose();
		new Partie();
	}

	/** 
	 * Ouvre une pop-up afin d'assurer la fermeture du programme, puis termine le programme
	 * @param _v Fen�tre sur laquelle centrer la pop-up
	 */
	public void quitterProg(ViewMenu _v) {
		int rep=JOptionPane.showConfirmDialog(_v,"Veux-tu vraiment quitter le programme ?","Quitter QUARTO :(",JOptionPane.YES_NO_OPTION);	
		if(rep == 0) {
			System.exit(0);
		}
	}
	
	/**
	 * Affiche une fen�tre pour modifier les param�tres
	 * @param _v
	 */
	public void modifParam(JFrame _v) {
		new ViewOption(_v, chargerParam());
	}
	
	/**
	 * Lis les param�tres pr�c�demment sauvegard�s dans un fichier
	 * @return Une instance param�tre contenant les param�tres � utiliser
	 */
	public static Parametre chargerParam() {
		Scanner sc = null;
        try {
            try {
                sc = new Scanner(new File("config/parametres.qt"));
                while (sc.hasNextLine()) {
                	char[] line = sc.next().toCharArray();
                	return (new Parametre(
                			Integer.parseInt(String.valueOf(line[0])),
                			Integer.parseInt(String.valueOf(line[1]))));
                }
            } finally {
                if (sc != null)
                    sc.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        return null;
	}
	
	/**
	 * Enregistre les param�tres dans un fichier
	 * @param _p Instance param�tre � sauvegarder
	 * @throws IOException
	 */
	public static void sauverParam(Parametre _p) throws IOException {
        PrintWriter pWriter = new PrintWriter(new FileWriter("config/parametres.qt", false));
        pWriter.print(_p.toString());
        pWriter.close() ;
	}
}
